import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import questionCtrl from '../controllers/question.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/questions - Get list of questions */
  //.get(questionCtrl.list)

  /** POST /api/questions - Create new question */
  .post(validate(paramValidation.createQuestion), questionCtrl.create);

//router.route('/:questionId')
  /** GET /api/questions/:questionId - Get question */
  //.get(questionCtrl.get)

  /** PUT /api/questions/:questionId - Update question */
  //.put(validate(paramValidation.updateQuestion), questionCtrl.update)

  /** DELETE /api/questions/:questionId - Delete question */
  //.delete(questionCtrl.remove);

/** Load question when API with questionId route parameter is hit */
//router.param('questionId', questionCtrl.load);

export default router;