import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * User Schema
 */
const QuestionSchema = new mongoose.Schema({
  content: {
    type: String,
    required: true
  },
  afirmative: {
    type: String,
    required: true
  },
  negative: {
    type: String,
    required: true
  },
  tags: [{
    type: String,
    match: [/^\S+$/, 'Error, only single word tags are allowed']
  }],
  createdAt: {
    type: Date,
    default: Date.now
  }
});



/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
QuestionSchema.method({
});

/**
 * Statics
 */
QuestionSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  // get(id) {
  //   return this.findById(id)
  //     .exec()
  //     .then((user) => {
  //       if (user) {
  //         return user;
  //       }
  //       const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
  //       return Promise.reject(err);
  //     });
  // },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  // list({ skip = 0, limit = 50 } = {}) {
  //   return this.find()
  //     .sort({ createdAt: -1 })
  //     .skip(+skip)
  //     .limit(+limit)
  //     .exec();
  // }
};

/**
 * @typedef User
 */
export default mongoose.model('Question', QuestionSchema);