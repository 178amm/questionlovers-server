import Question from '../models/question.model';

/**
 * Load question and append to req.
 */
// function load(req, res, next, id) {
//   Question.get(id)
//     .then((question) => {
//       req.question = question; // eslint-disable-line no-param-reassign
//       return next();
//     })
//     .catch(e => next(e));
// }

/**
 * Get question
 * @returns {Question}
 */
// function get(req, res) {
//   return res.json(req.question);
// }

/**
 * Create new question
 * @property {string} req.body.questionname - The questionname of question.
 * @property {string} req.body.mobileNumber - The mobileNumber of question.
 * @returns {Question}
 */
function create(req, res, next) {
  const question = new Question({
    content: req.body.content,
    afirmative: req.body.afirmative,
    negative: req.body.negative,
    tags: req.body.tags
  });

  question.save()
    .then(savedQuestion => res.json(savedQuestion))
    .catch(e => next(e));
}

/**
 * Update existing question
 * @property {string} req.body.questionname - The questionname of question.
 * @property {string} req.body.mobileNumber - The mobileNumber of question.
 * @returns {Question}
 */
// function update(req, res, next) {
//   const question = req.question;
//   question.questionname = req.body.questionname;
//   question.mobileNumber = req.body.mobileNumber;

//   question.save()
//     .then(savedQuestion => res.json(savedQuestion))
//     .catch(e => next(e));
// }

/**
 * Get question list.
 * @property {number} req.query.skip - Number of questions to be skipped.
 * @property {number} req.query.limit - Limit number of questions to be returned.
 * @returns {Question[]}
 */
// function list(req, res, next) {
//   const { limit = 50, skip = 0 } = req.query;
//   Question.list({ limit, skip })
//     .then(questions => res.json(questions))
//     .catch(e => next(e));
// }

/**
 * Delete question.
 * @returns {Question}
 */
// function remove(req, res, next) {
//   const question = req.question;
//   question.remove()
//     .then(deletedQuestion => res.json(deletedQuestion))
//     .catch(e => next(e));
// }

export default { create }; //load, get, create, update, list, remove };
